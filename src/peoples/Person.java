package peoples;

public class Person {

  String name;
  String birthday;

  public String getName() {
    return name;
  }

  public Person(String name, String birthday) {
    this.name = name;
    this.birthday = birthday;
  }

  @Override
  public String toString() {
    return "Person{" + "name=" + name + " birthday=" + birthday + "}";
  }
}

