package peoples;

public class Employee extends Person {

  private double salary;

  public double getSalary() {
    return salary;
  }

  public Employee(String name, String birthday, double salary) {
    super(name, birthday);
    this.salary = salary;
  }

  @Override
  public String toString() {
    return "Salary{" + "salary =" + salary + "}" + super.toString();
  }
}
