

import peoples.Employee;
import peoples.Person;

public class TestPerson {

  public static void main(String[] args) {
    Employee nv1 = new Employee("Nguyen Ha An", "03-07-1993", 100000);
    Manager nv2 = new Manager("Nguyen Minh Phuong", "03-01-1993", 15000);
    nv2.setAssistant((Employee) nv1);

    Person[] ds = new Person[3];

    ds[0] = nv1;
    ds[1] = nv2;
    ds[2] = new Person("Tran Minh Ngoc", "02-06-1993");

    for (int i = 0; i < ds.length; i++) {

      System.out.println(ds[i].toString());

    }
  }
}

