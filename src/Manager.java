import peoples.Person;
import peoples.*;

public class Manager extends Employee {

  Employee assistant;


  public Manager(String name, String birthday, double salary) {
    super(name, birthday, salary);
  }

  public void setAssistant(Employee assistant) {
    this.assistant = assistant;
    System.out.println("Manager " + this.getName() + " assistant " + assistant);
  }

  @Override
  public String toString() {
    return "Manager{" + super.toString() + '}';

  }

}
